
# MS Deploy Guide Notes.

# v1.0.0.0

- #### for next build
    * 1.0.9.4


- #### future tasks
    *  1.0.8.6 for guest table when checkout as guest implements
    * remove payment_verify verify & pdf colume based on 1.0.9.0 & also remove related entity mapp & api
      when admin implement new changes 
    * remove admin old api for get all space & get all booking when admin live deployed
    * remove space_additional_details secound contact persons details
    * run 1.0.9.3 when external api integrated add

	
- #### guides
	* any time issue check server time zone
		*timedatectl
		*sudo timedatectl set-timezone Asia/Colombo
	*for email change allow from email
		*https://www.google.com/settings/security/lesssecureapps
		
		
		
ALTER TABLE `mspaces_live`.`space_contact_persons` 
CHANGE COLUMN `contact_person_name` `contact_person_name` VARCHAR(60) NULL DEFAULT NULL ,
CHANGE COLUMN `contact_person_email` `contact_person_email` VARCHAR(254) NULL DEFAULT NULL ;

ALTER TABLE `mspaces_live`.`user` 
CHANGE COLUMN `name` `name` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `email` `email` VARCHAR(254) NOT NULL ;	