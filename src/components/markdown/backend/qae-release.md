
# MS Backend QAE Release Notes.

# v1.0.0.0


- #### v1.0.2.1

	* 24'th April 2018
		* IMPROVEMENT:EventSpace soft delete & priority 
		* BUGFIX: muli block with same time range can't book bug fixed

- #### v1.0.2.0

	* 23'rd April 2018
		* IMPROVEMENT:Code improvement
		* IMPROVEMENT:Advance Batch Search API
		* BUGFIX:linked spaces pass parent space also

- #### v1.0.1.9

	* 18'th April 2018
		* BUGFIX: 2nd contact person update not working
		* IMPROVEMENT: add review by email without user loged in api added
		* IMPROVEMENT: booking sms's cc to admin & leave a review cc to admin 
		
- #### v1.0.1.8

	* 2'nd April 2018
		* IMPROVEMENT: approved value attached with space details
		* IMPROVEMENT: get a space reviews api added
		* IMPROVEMENT: Available check with space approved also
		* IMPROVEMENT: 
		
- #### v1.0.1.7

	* 27th Mar 2018
		* BUGFIX: promoted space cancellation not working issue resolved
	    * IMPROVEMENT: MS-653 Add payment method in guest confirmation email
		* IMPROVEMENT: promotion email orgainzation name change to space name
		
- #### v1.0.1.6

	* 26th Mar 2018
		* BUGFIX: space update charge update bug fixed
		* IMPROVEMENT:space promotion start & end date added
		* IMPROVEMENT: promotion related payment action api added

		
- #### v1.0.1.5

	* 23th Mar 2018
        * IMPROVEMENT: dto added with get all guest bookings
		* IMPROVEMENT: promotion email & sms added
		
- #### v1.0.1.4

	* 21th Mar 2018
        * IMPROVEMENT: send sms & email to all contact persons
		* IMPROVEMENT: bcc & send email  changed for live
		* IMPROVEMENT: view review email also bcc to ms admin
		* IMPROVEMENT: cookie expire time chane from browser close to after 10 years
		* BUGFIX: starting price for guest based block spaces changed from min price to  min price *min num of guest
		* IMPROVEMENT: review new fileds added (msExperience)
		* IMPROVEMENT: space promotion real email contents added
		
- #### v1.0.1.3
		
    * 20th Mar 2018
        * BUGFIX: MA-1 guest payment verify & payment sent to host verify api's updated
		* NEW FEATURE: multi space contact person details feature
		* BUGFIX:admin CancellationCutOffDate calculation changed
		* IMPROVEMENT:dummy email for promoted spaces
		
- #### v1.0.1.2

    * 16th Mar 2018
        * BUGFIX: Bug Fix:sms for space listed  text fixed
		* NEW FEATURE: promoted spaces feature added
		* BUGFIX: space of the week get api added

- #### v1.0.0.2

    * 14th Mar 2018
        * BUGFIX: MS-632 Secondary contact person addition email2 added
		* BUGFIX: on admin booking details get reaerverstion status as confirm  instant of payment done
		* BUGFIX: user role FUSER  created for finance related users 

- #### v1.0.0.1

    * 13th Mar 2018
        * BUGFIX: Bug Fix:Ms-630 get a host booking error fixed


		