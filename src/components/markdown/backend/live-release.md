
# MS Backend Live Release Notes.

# v1.0.0.0

- #### v1.0.1.6

	* 24'th APril 2018
		* IMPROVEMENT:EventSpace soft delete & priority 
		* BUGFIX: muli block with same time range can't book bug fixed


- #### v1.0.1.5

	* 23'rd APril 2018
		* IMPROVEMENT:Code improvement
		* IMPROVEMENT:Advance Batch Search API
		* BUGFIX:linked spaces pass parent space also

- #### v1.0.1.4
    * 18'th APril 2018

		* IMPROVEMENT:approved value attached with space details
		* IMPROVEMENT: get a space reviews api added
		* IMPROVEMENT: Available check with space approved also
		* BUGFIX: 2nd contact person update not working
		* IMPROVEMENT: add review by email without user loged in api added
		* IMPROVEMENT: booking sms's cc to admin & leave a review cc to admin 

		
- #### v1.0.1.3

    * 27'th Mar 2018
        * BUGFIX: promoted space cancellation not working issue resolved
		* BUGFIX: space update charge update bug fixed
		* IMPROVEMENT:space promotion start & end date added
		* IMPROVEMENT: promotion related payment action api added
	    * IMPROVEMENT: MS-653 Add payment method in guest confirmation email
		* IMPROVEMENT: promotion email orgainzation name change to space name
			
- #### v1.0.1.2

    * 23'rd Mar 2018
        * BUGFIX: Bug Fix:sms for space listed  text fixed
		* NEW FEATURE: promoted spaces feature added
		* NEW FEATURE: space of the week get api added
	    * BUGFIX: MA-1 guest payment verify & payment sent to host verify api's updated
		* IMPROVEMENT: multi space contact person details feature & email,sms to all 
		* BUGFIX: admin CancellationCutOffDate calculation changed
		* IMPROVEMENT: view review email also bcc to ms admin
		* IMPROVEMENT: bcc & send email  changed for live
		* IMPROVEMENT: cookie expire time chanGE from browser close to after 10 years
		* BUGFIX: starting price for guest based block spaces changed from min price to  min price *min num of guest
		* IMPROVEMENT: review new fileds added (msExperience)

		
- #### v1.0.0.2

    * 14'th Mar 2018
        * BUGFIX: Bug Fix:MS-632 Secondary contact person addition email2 added
		* BUGFIX: Bug Fix:on admin booking details get reaerverstion status as confirm  instant of payment done
		* BUGFIX: Bug Fix:user role FUSER  created for finance related users 
        
- #### v1.0.0.1

    * 13'th Mar 2018
        * BUGFIX: Bug Fix:Ms-630 get a host booking error fixed

		
