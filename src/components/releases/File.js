import React from "react";
import marked from 'marked';

/**
 * Class representing File component.
 */
class File extends React.Component {

    state = {}

    componentWillMount() {

        const projects = {
            backend: {
                qa: require("../markdown/backend/qae-release.md"),
                live: require("../markdown/backend/live-release.md"),
                dg: require( "../markdown/backend/deploy-guides.md")
            },
            web: {
                qa: require("../markdown/web/qae-release.md"),
                live: require("../markdown/web/live-release.md"),
            },
            host: {
                qa: require("../markdown/hostEdit/qae-release.md"),
                live: require("../markdown/hostEdit/live-release.md"),
            },
            admin: {
                qa: require("../markdown/admin/qae-release.md"),
                live: require("../markdown/admin/live-release.md"),
            }
        }


        let releaseNotes = undefined;

        const { env, proj } = this.props;

        releaseNotes = projects[proj][env];

        fetch(releaseNotes)
            .then(response => {
                return response.text()
            })
            .then(text => {
                this.setState({
                    markdown: marked(text)
                });
            }).catch(err => {
                console.log(err)
            });
    }

    render() {

        const { markdown } = this.state;


        return (
            [<div style={{margin: 10, border: '1px dashed #e2e2e2', padding: 10}}>
                <h3>Project: {this.props.proj}</h3>
                <h3>Environment: {this.props.env}</h3>
            </div>,
            <article style={{ background: '#fff', padding: 20 }} dangerouslySetInnerHTML={{ __html: markdown ? markdown : 'Nothing yet, will be updated soon' }}></article >]
        )
    }

}

export default File;